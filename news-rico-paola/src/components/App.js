import React from "react";
import logo from '../img/logonews.svg';
import Search from "../containers/SearchContainer";
import News from "../containers/News";
import SearchMenu from "../components/SearchMenu"
import '../styles.css';
import {   Route, Switch, Redirect} from "react-router-dom";

const App = () => {
  return(
  <div className="App">
    <div className="infoHeader">
      <img src={logo} height="80" alt="logo"/>
      <Search></Search>
    </div>
 
    <div className="App-intro">
      <SearchMenu></SearchMenu> 
      <Switch>
        <Route  path="/news/category/:category"  component={News} />
        <Route  path="/news/search/:category" component={News}/>
        {/* <Route path="/news/category/:category" component={News}/> */}
        <Redirect from='/' to='/news/category/home' />
      </Switch>
    </div>
    
   
  </div>
)};

export default App;
