import React, {useState} from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";


const SearchComponent = () => {

  const [inputValue,setInputValue] = useState("");
 
  return (
    <React.Fragment>
       <div className="search">
      
         
          <Link className="input-group-prepend" to={`/news/search/${inputValue}`}> 
            <form className="search-container" >
                 <input id="search-box" type="text" value={inputValue} onChange={(e)=>setInputValue(e.target.value)} className="search-box"  />
              <input type="submit" id="search-submit"/>
            </form>
          </Link>
        
      </div>
    </React.Fragment>
  );
};

SearchComponent.propTypes = {
  defaultUser: PropTypes.string,
  onGet: PropTypes.func,
  onGetDate: PropTypes.func,
  getSearch:PropTypes.func
};

export default SearchComponent;