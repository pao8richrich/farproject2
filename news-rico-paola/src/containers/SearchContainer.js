import { connect } from "react-redux";
import { getNews, getNewsDate, getSearch } from "../actions";

import SearchComponent from "../components/SearchComponent"

const mapStateToProps = ( ownProps) => ({
  defaultUser: ownProps.user
});


const mapDispatchToProps = (dispatch) => ({
  onGet: (input) => dispatch(getNews(input)),
  onGetDate: (input) => dispatch(getNewsDate(input)),
  getSearch: (input) => dispatch(getSearch(input))
});
export default connect(mapStateToProps, mapDispatchToProps)(SearchComponent);
